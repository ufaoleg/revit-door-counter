﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using System.Xml.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Fabrication;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Microsoft.Win32;

namespace RevitDoorCounter
{
    /// <summary>
    /// Helper class to store info about doors
    /// </summary>
    class DoorGroup
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public int Count => Doors.Count;

        public HashSet<int> Doors { get; } = new HashSet<int>();

        public DoorGroup(int id, string name, string category)
        {
            Id = id;
            Name = name;
            Category = category;
        }

        public DoorGroup()
        {
            
        }

        public override string ToString()
        {
            return $"{Name}[{Category}] = {Doors.Count}";
        }
    }

    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class RevitDoorCounter : IExternalCommand
    {
        public Result Execute(
            ExternalCommandData commandData,
            ref string message,
            ElementSet elements)
        {
            //Get application and document objects
            var uiApp = commandData.Application;
            var doc = uiApp.ActiveUIDocument.Document;

            var result = GetDoorsOfGroup(doc);
            var stats = result.Select(x => x.Value).OrderBy(x => x.Category).ThenBy(x => x.Name).ToArray();
            ShowDialog(stats, doc);

            return Result.Succeeded;
        }

        /// <summary>
        /// Returns the doors info
        /// </summary>
        Dictionary<int, DoorGroup> GetDoorsOfGroup(Document doc)
        {
            Dictionary<int, DoorGroup> doors = new Dictionary<int, DoorGroup>();
            FilteredElementCollector collector = new FilteredElementCollector(doc);

            collector.OfCategory(BuiltInCategory.OST_Doors);
            StringBuilder sb = new StringBuilder();
            foreach (Element elem in collector)
            {
                var f1 = elem as FamilySymbol;
                if (f1 != null)
                    doors[f1.Id.IntegerValue] = new DoorGroup(f1.Id.IntegerValue, f1.Name, f1.FamilyName); ;
            }

            foreach (var elem in collector)
            {
                var f2 = elem as FamilyInstance;
                if (f2 == null) continue;
                var typeId = f2.GetTypeId()?.IntegerValue ?? -1;
                DoorGroup dg;
                if (!doors.TryGetValue(typeId, out dg))
                {
                    dg = new DoorGroup(typeId, "unknown", "unknown");
                    doors[typeId] = dg;
                }
                dg.Doors.Add(f2.Id.IntegerValue);
            }
            return doors;
        }


        private void ShowDialog(DoorGroup[] stats, Document doc)
        {
            var w = new Window()
            {
                Width = 300,
                Height = 300,
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };
            var dp = new DockPanel();
            var btn = new Button()
            {
                Content = "Сохранить...",
                Margin = new Thickness(5),
            };
            RoutedEventHandler handler = (sender, args) =>
            {
                SaveAsXml(stats, doc);
                w.Close();
            };
            btn.Click += handler;
            DockPanel.SetDock(btn, Dock.Bottom);
            var list = new ListView()
            {
                ItemsSource = stats
            };
            var gw = new GridView();
            gw.Columns.Add(new GridViewColumn()
            {
                Header = "Название",
                Width = 100,
                DisplayMemberBinding = new System.Windows.Data.Binding(nameof(DoorGroup.Name))
            });
            gw.Columns.Add(new GridViewColumn()
            {
                Header = "Категория",
                Width = 100,
                DisplayMemberBinding = new System.Windows.Data.Binding(nameof(DoorGroup.Category))
            });
            gw.Columns.Add(new GridViewColumn()
            {
                Header = "Количество",
                Width = 100,
                DisplayMemberBinding = new System.Windows.Data.Binding(nameof(DoorGroup.Count))
            });
            list.View = gw;
            dp.Children.Add(btn);
            dp.Children.Add(list);
            w.Content = dp;

            w.ShowDialog();
            btn.Click -= handler;
        }

        private void SaveAsXml(DoorGroup[] stats, Document doc)
        {
            try
            {
                var dlg = new SaveFileDialog
                {
                    FileName = Path.GetFileNameWithoutExtension(doc.Title) + "_DOORS.xml",
                    DefaultExt = "xml"
                };

                if (dlg.ShowDialog() != true)
                    return;
                var root = new XElement("doors");
                root.Add(new XAttribute("project", doc.Title));

                foreach (var doorGroup in stats)
                {
                    var r = new XElement("door", doorGroup.Doors.Count);
                    r.Add(new XAttribute("name", doorGroup.Name));
                    r.Add(new XAttribute("category", doorGroup.Category));
                    root.Add(r);
                }

                var xdoc = new XDocument(root);
                using (var fs = new FileStream(dlg.FileName, FileMode.Create, FileAccess.Write))
                    xdoc.Save(fs);

            }
            catch (Exception e)
            {
                MessageBox.Show("Не удалось сохранить документ: " + e.Message);
            }

        }
    }
}
